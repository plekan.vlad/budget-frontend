import './App.scss'
import {Login} from './components/Login'
import { BrowserRouter as Router,Routes, Route, Link } from 'react-router-dom';
import logo from './img/logo.jpeg'
import { Register } from './components/Register';

function App() {
  return (
    <Router>

    <div className='app'>
      <div className="app__header">
          <Link to="/">
            <img src={logo} alt="" />
          </Link>
          <ul className='app__actions'>
            <li>
              <Link className='app__link' to="/login">Login</Link>
            </li>
            <li>
              <Link className='app__link' to="/register">Register</Link>
            </li>
          </ul>
      </div>
      <Routes>
        <Route exact path="/login" element={<Login/>}></Route>
        <Route exact path="/register" element={<Register/>}></Route>
      </Routes>
    </div>

    </Router>
  );
}

export default App;
