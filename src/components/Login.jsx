import React from "react"
import './Login.scss'
import login from '../img/login.png'
import {useNavigate} from 'react-router-dom'
class Login extends React.Component{

  constructor(){
    super()
    this.state ={
      username: '',
      password: '',
      error: null,
      isLoaded: false,
      access_token: '',
      refresh_token: '',
      loggined: '',
    }
  };

  
  render(){
    const {username, password} = this.state

    this.handleClick = (event) => {
      this.setState({[event.target.name]: event.target.value})
    }
    
    this.logIn = (e) => {

      e.preventDefault();
      // const {username, password} = this.state
      // if((!username || (username.length < 8 || username.length > 17)) || (!password || (password.length < 8 || password.length > 17))){
      //   alert("Fill the fields, they range is <8, 16>")
      //   return;
      // }

      const userData = {
        username: this.state.username,
        password: this.state.password,
      }
    
      fetch("https://lazy-budget-backend.herokuapp.com/auth/login", {
        mode: 'cors',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Origin': 'http://localhost:3000',
          
        },
        body: JSON.stringify(userData),
        
      })
      .then(res => res.json())
      .then(
        (result) => {
          localStorage.setItem(
            'access_token', result.access_token,
          )
          localStorage.setItem(
            'refresh_token', result.refresh_token,
          )
        },
      (error) => {
        this.setState({
          error, 
          isLoaded: true
        })
      })
    }
    return(
      <div className="login__container">
        <img src={login}></img>
        <span>Login</span>
        <div className="login__inputs">
          <input 
            type="text" 
            name="username" 
            placeholder='Username' 
            value={username}
            onChange={this.handleClick}
          />
          <input 
            type="password" 
            name="password" 
            placeholder='Password' 
            value={password}
            onChange={this.handleClick}
          />
          <button onClick={this.logIn}>Login</button>
        </div>
      </div>
    )
  }

}

export {Login}