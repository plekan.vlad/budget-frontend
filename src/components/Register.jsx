import React from "react"
import login from '../img/login.png'

class Register extends React.Component{

  constructor(){
    super()
    this.state ={
      username: '',
      password: '',
      error: null,
      isLoaded: false,
    }
  };

  
  render(){
    const {username, password} = this.state

    this.handleChange = (event) => {
      this.setState({[event.target.name]: event.target.value})
    }
    
    this.signUp = (e) =>{
      e.preventDefault();
      // const {username, password} = this.state
      // if((!username || (username.length < 8 || username.length > 17)) || (!password || (password.length < 8 || password.length > 17))){
      //   alert("Fill the fields, they range is <8, 16>")
      //   return;
      // }
      const userData = {
        username: this.state.username,
        password: this.state.password,
      }
      fetch("https://lazy-budget-backend.herokuapp.com/auth/register", {
        mode: 'cors',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Origin': 'http://localhost:3000',
          
        },
        body: JSON.stringify(userData),
        
      })
      .then(res => {
        console.log("Request from POST",res)
        console.log("Request from POST",res.body)
        this.setState({
          isLoaded: true
        })
        return res.json()
      })
      .then(body => {
        console.log("response json", body)
      })
      .catch(err => 
        console.log(err)
        )
      
    }
    
    return(
      <div className="login__container">
        <img src={login}></img>
        <span>Create account</span>
        <div className="login__inputs">
          <input 
            type="text" 
            name="username" 
            placeholder='Username' 
            value={username}
            onChange={this.handleChange}
          />
          <input 
            type="password" 
            name="password" 
            placeholder='Password' 
            value={password}
            onChange={this.handleChange}
          />
          <button onClick={this.signUp}>Register</button>
        </div>
      </div>
    )
  }

}

export {Register}